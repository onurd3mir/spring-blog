package demir.blog.domain.responses;

import java.sql.Timestamp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostListResponse {
    private int postId;
    private String postTitle;
    private String postContent;
    private Timestamp postDate;
    private String userName;

}
