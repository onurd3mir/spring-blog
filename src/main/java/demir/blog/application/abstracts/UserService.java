package demir.blog.application.abstracts;

import java.util.List;

import demir.blog.domain.entities.User;
import demir.blog.domain.responses.UserListResponse;

public interface UserService {
	
	List<UserListResponse> getAllUser();
	
	void addUser(User user);
}
