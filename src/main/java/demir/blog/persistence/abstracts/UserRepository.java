package demir.blog.persistence.abstracts;

import org.springframework.data.jpa.repository.JpaRepository;

import demir.blog.domain.entities.User;

public interface UserRepository extends JpaRepository<User, Integer> {

}
