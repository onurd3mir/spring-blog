package demir.blog.presentation.webUI.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import demir.blog.application.abstracts.PostService;
import demir.blog.domain.requests.CreatePostRequest;
import demir.blog.domain.requests.UpdatePostRequest;
import demir.blog.domain.responses.PostListResponse;

@Controller
public class PostsControllers {

    private final PostService postService;

    @Autowired
    public PostsControllers(PostService postService) {
        this.postService = postService;
    }

    @GetMapping({"addpost.html" })
    public ModelAndView addPost() {
        ModelAndView viewModel = new ModelAndView("/posts/add_post");
        CreatePostRequest post = new CreatePostRequest();
        viewModel.addObject("post", post);
        return viewModel;
    }

    @PostMapping("addpost")
    public String addPost(CreatePostRequest createPostRequest) {
        createPostRequest.setUserId(1);
        postService.addPost(createPostRequest);
        return "redirect:/posts.html";
    }

    @GetMapping({"posts.html"})
    public ModelAndView getAll() {
        ModelAndView viewModel = new ModelAndView("/posts/list");
        List<PostListResponse> posts = postService.getAll();
        viewModel.addObject("posts", posts);
        return viewModel;
    }

    @GetMapping("/posts/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable int id){
        postService.deletePostById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/posts/{id}/detail.html")
    public ModelAndView detail(@PathVariable int id){
        ModelAndView viewModel = new ModelAndView("/posts/detail");
        PostListResponse post = postService.getPostById(id);
        viewModel.addObject("post", post);
        return viewModel;
    }

    @GetMapping("/posts/{id}/edit.html")
    public ModelAndView edit(@PathVariable int id){
        ModelAndView viewModel = new ModelAndView("/posts/edit");
        PostListResponse post = postService.getPostById(id);
        UpdatePostRequest postModel = new UpdatePostRequest();
        postModel.setPostId(post.getPostId());
        postModel.setTitle(post.getPostTitle());
        postModel.setContent(post.getPostContent());
        viewModel.addObject("post", postModel);
        return viewModel;
    }

    @PostMapping("edit")
    public String edit(UpdatePostRequest postRequest) {
        postService.EditPost(postRequest); 
        return "redirect:/posts.html";
    }
}
