package demir.blog.application.abstracts;


import java.util.List;

import demir.blog.domain.requests.CreatePostRequest;
import demir.blog.domain.requests.UpdatePostRequest;
import demir.blog.domain.responses.PostListResponse;

public interface PostService {

     void addPost(CreatePostRequest createPostRequest);
     
     List<PostListResponse> getAll(); 
     
     void deletePostById(int id);

     PostListResponse getPostById(int id);
     
     void EditPost(UpdatePostRequest postRequest);
}
