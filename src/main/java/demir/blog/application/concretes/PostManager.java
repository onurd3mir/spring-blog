package demir.blog.application.concretes;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import demir.blog.application.abstracts.PostService;
import demir.blog.domain.entities.Post;
import demir.blog.domain.entities.User;
import demir.blog.domain.requests.CreatePostRequest;
import demir.blog.domain.requests.UpdatePostRequest;
import demir.blog.domain.responses.PostListResponse;
import demir.blog.persistence.abstracts.PostRepository;
import demir.blog.persistence.abstracts.UserRepository;

@Service
public class PostManager implements PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;

    public PostManager(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void addPost(CreatePostRequest createPostRequest) {
        User user = userRepository.findById(createPostRequest.getUserId()).orElseThrow();
        Post post = new Post();
        Date date = new Date();
        Timestamp getDate = new Timestamp(date.getTime());

        post.setTitle(createPostRequest.getTitle());
        post.setContent(createPostRequest.getContent());
        post.setUser(user);
        post.setCreateDate(getDate);

        postRepository.save(post);
    }

    @Override
    public List<PostListResponse> getAll() {
        List<Post> posts = postRepository.findAll();
        List<PostListResponse> postResponse = new ArrayList<PostListResponse>();

        for (Post post : posts) {
            PostListResponse item = new PostListResponse();
            item.setPostId(post.getPostId());
            item.setPostTitle(post.getTitle());
            item.setPostDate(post.getCreateDate());
            item.setUserName(post.getUser().getUserName());
            item.setPostContent(post.getContent());
            postResponse.add(item);
        }

        return postResponse;
    }

    @Override
    public void deletePostById(int id) {
        Post post = postRepository.findById(id).orElseThrow();
        postRepository.delete(post);
    }

    @Override
    public PostListResponse getPostById(int id) {
        Post post = postRepository.findById(id).orElseThrow();
        PostListResponse postResponse = new PostListResponse();
        postResponse.setPostId(post.getPostId());
        postResponse.setPostTitle(post.getTitle());
        postResponse.setPostContent(post.getContent());
        postResponse.setPostDate(post.getCreateDate());
        postResponse.setUserName(post.getUser().getUserName());
        return postResponse;
    }

    @Override
    public void EditPost(UpdatePostRequest postRequest) {
        Post post = postRepository.findById(postRequest.getPostId()).orElseThrow();
        post.setTitle(postRequest.getTitle());
        post.setContent(post.getContent());
        postRepository.save(post);
    }
}
