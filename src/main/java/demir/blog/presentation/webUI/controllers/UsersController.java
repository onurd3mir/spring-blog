package demir.blog.presentation.webUI.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import demir.blog.application.abstracts.UserService;
import demir.blog.domain.responses.UserListResponse;

@Controller
public class UsersController {
	
	private UserService userService;
	
	@Autowired
	public UsersController(UserService userService) {
		super();
		this.userService = userService;
	}

	@GetMapping({"/users.html","users-list.html"})
	public ModelAndView userList() {
		ModelAndView viewModel = new ModelAndView("users/list");
		List<UserListResponse> users = userService.getAllUser();
		viewModel.addObject("users", users);
		return viewModel;
	}



}
