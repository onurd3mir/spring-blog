package demir.blog.presentation.webApi.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demir.blog.application.abstracts.UserService;
import demir.blog.domain.responses.UserListResponse;

@RestController
@RequestMapping("/api/users")
public class UsersControllers {

    private final UserService userService;
    
    @Autowired
    public UsersControllers(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/getall")
    public List<UserListResponse> getAll(){
        return userService.getAllUser();  
    }

}
