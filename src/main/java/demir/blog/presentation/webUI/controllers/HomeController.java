package demir.blog.presentation.webUI.controllers;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


import demir.blog.application.abstracts.PostService;
import demir.blog.domain.responses.PostListResponse;

@Controller
public class HomeController {

	private final PostService postService;

	public HomeController(PostService postService) {
		this.postService=postService;
	}

	@RequestMapping("/")
	public ModelAndView index() {
		ModelAndView viewModel = new ModelAndView("/home/index");
        List<PostListResponse> posts = postService.getAll();
        viewModel.addObject("posts", posts);
        return viewModel;
	}
}
