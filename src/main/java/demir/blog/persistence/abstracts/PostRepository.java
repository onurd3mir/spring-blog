package demir.blog.persistence.abstracts;

import org.springframework.data.jpa.repository.JpaRepository;

import demir.blog.domain.entities.Post;

public interface PostRepository extends JpaRepository<Post, Integer> {

}
