package demir.blog.application.concretes;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demir.blog.application.abstracts.UserService;
import demir.blog.domain.entities.User;
import demir.blog.domain.responses.UserListResponse;
import demir.blog.persistence.abstracts.UserRepository;

@Service
public class UserManager implements UserService {

	private UserRepository userRepository;
	
	@Autowired
	public UserManager(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public List<UserListResponse> getAllUser() {
		List<User> users = userRepository.findAll();
		List<UserListResponse> userResponse =  new ArrayList<UserListResponse>();
		
		for(User user : users) {
			UserListResponse item= new UserListResponse();
			item.setId(user.getUserId());
			item.setUserName(user.getUserName());
			userResponse.add(item);
		}
		
		return userResponse;
	}

	@Override
	public void addUser(User user) {
		userRepository.save(user);
		
	}

}
